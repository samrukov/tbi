const http = require('http');
const request = require('request-promise');
const token = '';
const accountNumber = '';
const express = require('express');

// const querystring  = require('querystring');
// const bodyParser = require('body-parser');

var app = express();
const jsonParser = express.json();

const hostname = '';
const port = 80;

app.listen(port, hostname, function(){
  console.log('server was started');

  // sendRequestPOST('32354', 'samrukov@gmail.com', 'Голос', 'Вася', 'Иванов', 6000).then(data =>{
  //   console.log("data2:", data);
  // });

  // let result = sendRequestPOST('32354', 'samrukov@gmail.com', 'Голос', 'Вася', 'Иванов', 6000);
  // console.log(result);
});

app.get("/api/users", function(req, res){
  let users = {user1: 'damir', user2: 'kolya'};
  res.send(users);
})

app.post("/api/sendbill", jsonParser, function (req, res) {
  if(!req.body) return res.sendStatus(400);
  let name = req.body.name;
  let surname = req.body.surname;
  let email = req.body.email;
  let competition = req.body.competition;
  let billNumber = req.body.billNumber;
  let price = req.body.price;

  sendRequestPOST(billNumber, email, competition, name, surname, price).then(data =>{
    console.log("data2:", data);
    res.send(data);
  });
  // res.sendStatus(200);
});

async function sendRequestPOST(billNumber, email, competition, name, surname, price){
    var url = 'https://business.tinkoff.ru/openapi/api/v1/invoice/send';
    var body = {
      "invoiceNumber": billNumber,
      // "dueDate": "2021-2-20",
      // "invoiceDate": "2021-2-18",
      "accountNumber": accountNumber,
      "payer": {
        "name": `${name} ${surname}`
        // "inn": "730990470834",
        // "kpp": "123456789"
      },
      "items": [
        {
          "name": `Участие в конкурсе "${competition} "`,
          "price": price,
          "unit": "Шт",
          "vat": "None",
          "amount": 1
        }
      ],
      "contacts": [
        {
          "email": email
        }
      ]
    }

  const options = {
    url: url, 
    method: 'POST',
    headers: {
      // 'User-Agent': 'request',
      'contentType': "application/json; charset=UTF-8",
      'accept': 'application/json',
      'Authorization': 'Bearer ' + token
    },
    body: JSON.stringify(body)
    // json: true
  };
  
  var result = "result";
  await request(options)
    .then((data) => {
      console.log("data1", data);
      result = data;
    })
    .catch(function (err) {
      console.log(err.stack)
    })
    return result
}

function sendRequestGetBill(){
  var url = `https://business.tinkoff.ru/openapi/api/v1/bank-statement?accountNumber=${accountNumber}&from=2021-02-12&till=2021-02-12`;
  const options = {
    url: url,
    method: 'GET',
    headers: {
      // 'User-Agent': 'request',
      // 'contentType': "application/json; charset=UTF-8",
      'accept': 'application/json',
      'Authorization': 'Bearer ' + token
    }
  };
  
  function callback(error, response, body) {
    console.error('error:', error);
    console.log('statusCode:', response.statusCode);
    // let r = JSON.parse(body);
    console.log('body:', body);
    sendRequestGoogle(body);
  }

  request(options, callback);
}

function sendRequestGoogle(body){
  var url = "https://script.google.com/macros/s/AKfycbz49c4G1TsgiU67dK4Z8QNL78WEbgQJcyNmBDYdy61Vrhzi8Wyzup61/exec";
  const options = {
    url: url,
    method: 'POST',
    headers: {
      // 'User-Agent': 'request',
      'contentType': "application/json; charset=UTF-8",
      'accept': 'application/json'
    },
    body: JSON.stringify(body)
  };
  function callback(error, response, body) {
    console.error('error:', error);
    console.log('statusCode:', response.statusCode);
    // let r = JSON.parse(body);
    // console.log('body:', body);
  }
  request(options, callback);
}

/*
const server = http.createServer((req, res) => {
  if (req.url != '/favicon.ico') {
    console.info("start");

    // sendRequestPOST();
    // sendRequestGet();
    // sendRequestGetBill();

    console.info("finish");

    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    res.end('server started');
  }
});
*/

/*
server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`); 

  // var url = "https://script.google.com/macros/s/AKfycbz49c4G1TsgiU67dK4Z8QNL78WEbgQJcyNmBDYdy61Vrhzi8Wyzup61/exec";
  // // var url = 'https://jsonplaceholder.typicode.com';
  // var bodyOut = { name: "Damir", age: 32, gender: "man" };
  // // var bodyOut = { title: "foo", body: 'bar', userId: 1 };

  // // request(options, callback);
  // sendRequest(url, bodyOut)
});
*/